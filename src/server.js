const express = require('express');

const app = express();
const server = require('http').Server(app);
const path = require('path')

app.use(express.static(__dirname + '/../build/'));

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname + '/../build/index.html'));
});

return server.listen(process.env.PORT || 8080, function () {
    console.log(`Server is on, listening on: ${process.env.PORT || 8080}`);
});