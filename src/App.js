import React, { Component } from "react";
import './App.css';
import api from "./services/api.js"
import handle from "./services/handleError.js"
import { tada } from 'react-animations';
import Radium, { StyleRoot } from 'radium';
import capture from './assets/capture.png';
import sentinel from './assets/sentinel.png';
import scan from './assets/scan.png';
import android from './assets/Android.png';


const styles = {
  tada: {
    animation: 'x 1s',
    animationName: Radium.keyframes(tada, 'tada')
  }
}

class App extends Component {

  constructor() {
    super()
    this.ref = React.createRef()
    this.state = {
      catetoOposto: 0,
      catetoAdjacente: 0,
      hypotenuse: 0,
      ishypotenuseHidden: true,
      cromaiLink: "",
      cromaiProduct: "",
      unit: "m",
      isInformationHidden: true,
      unitConverted: "",
      text: "",
      cromaiPicture: capture
    }
    this.hiponetusaRequest = this.hiponetusaRequest.bind(this);

  }

  myChangeHandler = (event) => {
    let cateto = event.target.name;
    var val = event.target.value;
    val = val.replace(/^0+/, '')
    if (val.length === 0) {
      val = 0
    }
    this.setState({ [cateto]: val })
  }

  async hiponetusaRequest(event) {
    event.preventDefault();
    this.setState({ ishypotenuseHidden: true });
    this.setState({ isInformationHidden: true });
    console.log(this.state)
    const body = {
      cat_op: parseInt(this.state.catetoOposto),
      cat_adj: parseInt(this.state.catetoAdjacente)
    }
    const [response, err] = await handle(api.post("calcula", body));
    if (err) {
      const { response } = err;
      response.data ? alert(response.data.message) : alert("Internal server error")
    } if (response.data.status_code === 400) {
      alert(response.data.mensagem)
    }
    else {
      console.log(response)
      this.setState({ hypotenuse: response.data.hip });
      this.setState({ ishypotenuseHidden: false });
      let hypotenuseLength = Math.ceil(Math.log10(response.data.hip + 1))
      console.log(hypotenuseLength)
      switch (hypotenuseLength) {
        case 1:
          this.setState({ unit: "m" });
          break;
        case 2:
          this.setState({ unit: "dm" });
          break;
        case 3:
          this.setState({ unit: "cm" });
          break;
        case 5:
          this.setState({ unit: "dm" });
          break;
        case 6:
          this.setState({ unit: "cm" });
          break;
        default:
          this.setState({ unit: "mm" });
          break;

      }
      if (hypotenuseLength > 0) {
        this.setState({ isInformationHidden: false });
        if (hypotenuseLength > 7) {
          this.setState({ cromaiLink: "https://www.cromai.com/scan" });
          this.setState({ cromaiProduct: "Conheça o Scan" });
          this.setState({ unitConverted: Math.round(response.data.hip / (Math.pow(10, 6)) * 10) / 10 + " Quilômetros" })
          this.setState({ text: "De acordo com minhas análises, possivelmente a praga atingiu quase toda sua fazenda, por isso, recomendo utilizar o Cromai Scan para descobrir o tipo de praga e resolver seu problema." })
          this.setState({ cromaiPicture: scan })
        } else if (hypotenuseLength > 4) {
          this.setState({ cromaiLink: "https://www.cromai.com/sentinel" });
          this.setState({ cromaiProduct: "Conheça o Sentinel" });
          this.setState({ unitConverted: Math.round((response.data.hip / (Math.pow(10, hypotenuseLength - 1))) * 10) / 10 + " Quilômetros" })
          this.setState({ text: "De acordo com minhas análises, possivelmente a praga atingiu boa parte da sua lavoura, por isso, recomendo utilizar o Cromai Sentinel para descobrir o tipo de praga e resolver seu problema." })
          this.setState({ cromaiPicture: sentinel })
        } else {
          this.setState({ cromaiLink: "https://www.cromai.com/capture" });
          this.setState({ cromaiProduct: "Conheça o Capture" });
          this.setState({ unitConverted: Math.round(response.data.hip / (Math.pow(10, hypotenuseLength - 1)) * 10) / 10 + " Metros" })
          this.setState({ text: "De acordo com minhas análises, possivelmente a praga atingiu apenas uma de suas plantas e, por isso, recomendo utilizar o Cromai Capture para descobrir o tipo de praga e resolver seu problema." })
          this.setState({ cromaiPicture: capture })
        }
      }
    }
  }

  render() {
    return (
      <div className="App">
        <div className="informationDiv">
          <h1>Tudo é número</h1>
          <h3 style={{ maxWidth: 800 }}>Pitágoras, sei que o motivo de sua visita nesse templo é devido suas incertezas.<br />Enquanto todos na Grécia plantam Uvas, Azeitonas e Vegetais, você segue plantando Cana-de-Açucar, Café e Soja.<br /><br />A baixa produtividade de sua fazenda pode estar ligado ao índice de pragas na sua plantação, por isso estou aqui para te ajudar.<br /><br />Diga-me o cateto oposto e o cateto adjacente que lhe direi o tamanho do seu problema e indicarei uma sugestão para resolvê-lo.</h3>
          <form>
            <div>
              <p className="catetos">Cateto Oposto</p>
              <input type="number" autoFocus min="0" step="1" name="catetoOposto" value={this.state.catetoOposto} onChange={this.myChangeHandler} />
              <p className="catetos"> Cateto Adjacente</p>
              <input type="number" min="0" step="1" name="catetoAdjacente" value={this.state.catetoAdjacente} onChange={this.myChangeHandler} />
            </div>
            <button onClick={this.hiponetusaRequest.bind(this)}>Enviar</button>
            <StyleRoot>
              <h2 hidden={this.state.ishypotenuseHidden} style={styles.tada}>Hipotenusa: {this.state.hypotenuse} {this.state.unit}</h2>
            </StyleRoot>
          </form>

          <div hidden={this.state.isInformationHidden}>
            <h3>As pragas estão ocupando apróximadamente = {this.state.unitConverted} da sua lavoura</h3>
            <h4 style={{ maxWidth: 800 }}>{this.state.text}</h4>
            <img src={this.state.cromaiPicture} alt=""/>
            <a href={this.state.cromaiLink} className="cromaiButton" target="_blank" rel="noopener noreferrer">{this.state.cromaiProduct}</a>
          </div>
          </div>
        <footer>
          <p className="catetos">Acesse também nosso aplicativo Android:
          <a href="https://drive.google.com/open?id=11uQpgbauMAC2-JZnPPD0souP9rh1oLOq" target="_blank" rel="noopener noreferrer">
              <img className="android" src={android} alt="app" />
            </a>
          </p>
        </footer>
      </div>
    );
  }
}

export default App;